require.config({
	paths: {
		jQuery: 'vendor/jquery/jquery-1.8.2.min',
		Underscore: 'vendor/documentcloud/underscore/underscore-min',
		Backbone: 'vendor/documentcloud/backbone/backbone-min'
	}
});

require(
	[
		'app',

		'order!vendor/jquery/jquery-1.8.2.min',
		'order!vendor/documentcloud/underscore/underscore-min',
		'order!vendor/documentcloud/backbone/backbone-min'
	],
	function (App) {
		App.initialise();
	}
);

