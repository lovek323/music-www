define([ 'router' ], function(Router) {
	function initialise() {
		Router.initialise();
	}

	return { initialise: initialise };
});

