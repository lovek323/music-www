define(
	[ 'views/artists/list' ],
	function(ArtistListView) {
		var AppRouter = Backbone.Router.extend({
			routes: {
				'artists': 'showArtists',
				'*actions': 'defaultAction'
			},
			showArtists: function() {
				var artistListView = new ArtistListView({el:'body'});
				artistListView.render(true);
			},
			defaultAction: function(actions) {
				console.log('No route: ' + actions);
			}
		});

		var initialise = function() {
			var app_router = new AppRouter();
			Backbone.history.start();
		};

		return { initialise: initialise };
	}
);

