define(
	[ 'models/artist' ],
	function(Artist) {
		var Artists = Backbone.Collection.extend({
			model: Artist,
			url: 'http://api.music/artists.json'
		});

		return Artists;
	}
);

