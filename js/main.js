requirejs.config({
	shim: {
		'app': {
			deps: [ 'libs/backbone/backbone-min' ],
			exports: 'App'
		},
		'libs/backbone/backbone-min': { deps: [ 'libs/underscore/underscore-min', 'libs/jquery/jquery-1.8.2.min' ] }
	},
});

require(
	[ 'app' ],
	function(App) {
		App.initialise();
	}
);

