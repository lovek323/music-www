define(function() {
	var Artist = Backbone.Model.extend({
		defaults: {
			urlRoot: 'http://api.music/artist.json?id='
		}
	});

	return Artist;
});

